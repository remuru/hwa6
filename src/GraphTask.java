import java.util.ArrayList;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 * Ülesanne 2: "Koostada meetod, mis teeb kindlaks, kas etteantud sidusas lihtgraafis leidub Euleri tsükkel, ning
 * selle leidumisel nummerdab kõik servad vastavalt nende järjekorrale Euleri tsükli läbimisel (vt. ka Fleury' algoritm)."
 * Eeskujuks võetud: https://www.geeksforgeeks.org/fleurys-algorithm-for-printing-eulerian-path/
 * https://github.com/GraphWalker/graphwalker-project/blob/master/graphwalker-core/src/main/java/org/graphwalker/core/algorithm/Fleury.java
 * https://code.google.com/archive/p/jalds/source/default/source
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        //Ristkülik
       /*
       Vertex a = new Vertex("A");
       Vertex b = new Vertex("B");
       Vertex c = new Vertex("C");
       Vertex d = new Vertex("D");
       g.first = a;
       a.next = b;
       b.next = c;
       c.next = d;
       Arc ab = new Arc("AB");
       Arc bc = new Arc("BC");
       Arc cd = new Arc("CD");
       Arc da = new Arc("DA");
       Arc ac = new Arc("AC");
       a.first = ab;
       b.first = bc;
       c.first = cd;
       d.first = da;
       ab.next = ac;
       ab.target = b;
       bc.target = c;
       cd.target = d;
       da.target = a;
       ac.target = c;
       enumerate(g);
       System.out.println(g);*/

        //Exception
      /* GraphTask task = new GraphTask();
       Vertex a = task.new Vertex("A");
       Vertex b = task.new Vertex("B");
       Vertex c = task.new Vertex("C");
       g.first = a;
       a.next = b;
       b.next = c;
       Arc ab = task.new Arc("AB");
       a.first = ab;
       ab.target = b;
       task.enumerate(g);*/

        //Kolmnurk
        /*
        GraphTask task = new GraphTask();
        Vertex a = task.new Vertex("A");
        Vertex b = task.new Vertex("B");
        Vertex c = task.new Vertex("C");
        g.first = a;
        a.next = b;
        b.next = c;
        Arc ab = task.new Arc("AB");
        Arc bc = task.new Arc("BC");
        Arc ca = task.new Arc("CA");
        a.first = ab;
        b.first = bc;
        c.first = ca;
        ab.target = b;
        bc.target = c;
        ca.target = a;
        task.enumerate(g);
        System.out.println(g);*/

        //Pentatope graph
        /*GraphTask task = new GraphTask();
        Vertex a = task.new Vertex("A");
        Vertex b = task.new Vertex("B");
        Vertex c = task.new Vertex("C");
        Vertex d = task.new Vertex("D");
        Vertex e = task.new Vertex("E");
        g.first = a;
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        Arc ab = task.new Arc("AB");
        Arc bc = task.new Arc("BC");
        Arc cd = task.new Arc("CD");
        Arc de = task.new Arc("DE");
        Arc ea = task.new Arc("EA");
        Arc ac = task.new Arc("AC");
        Arc ce = task.new Arc("CE");
        Arc eb = task.new Arc("EB");
        Arc bd = task.new Arc("BD");
        Arc da = task.new Arc("DA");
        a.first = ab;
        b.first = bc;
        c.first = cd;
        d.first = de;
        e.first = ea;
        ab.next = ac;
        cd.next = ce;
        ea.next = eb;
        bc.next = bd;
        de.next = da;
        ab.target = b;
        bc.target = c;
        cd.target = d;
        de.target = e;
        ea.target = a;
        ac.target = c;
        ce.target = e;
        eb.target = b;
        bd.target = d;
        da.target = a;
        task.enumerate(g);
        System.out.println(g);*/

        //liblikas
        GraphTask task = new GraphTask();
        Vertex a = task.new Vertex("A");
        Vertex b = task.new Vertex("B");
        Vertex c = task.new Vertex("C");
        Vertex d = task.new Vertex("D");
        Vertex e = task.new Vertex("E");
        g.first = a;
        a.next = b;
        b.next = c;
        c.next = d;
        Arc ab = task.new Arc("AB");
        Arc bc = task.new Arc("BC");
        Arc cd = task.new Arc("CD");
        Arc db = task.new Arc("DB");
        Arc be = task.new Arc("BE");
        Arc ea = task.new Arc("EA");
        a.first = ab;
        b.first = bc;
        c.first = cd;
        d.first = db;
        bc.next = be;
        e.first = ea;
        ab.target = b;
        bc.target = c;
        cd.target = d;
        db.target = b;
        be.target = e;
        ea.target = a;
        task.enumerate(g);
        System.out.println(g);


    }

    /**
     * Enumerate Eulerian graph arcs.
     *
     * @param g Eulerian graph.
     */
    public void enumerate(Graph g) {
        ArrayList<Vertex> vertices = g.vertices();
        int i = 0;
        for (Vertex vertex : vertices){
            if (vertex.info % 2 == 1)
                i++;
        if (i > 2)
            throw new RuntimeException("Graph " + g + " is not Eulerian graph.");
        if (vertex.info == 0)
            throw new RuntimeException("Graph " + g + " is not Eulerian graph.");}
        Vertex v = g.first;
        for (i = 0; i < g.arcs(); i++) {
            if (v.first.number == 0) {
                v.first.number = i + 1;
                v = v.first.target;
            } else {
                Arc a = v.first.next;
                if (a == null)
                    throw new RuntimeException("Graph " + g.id + " has no Eulerian path.");
                while (a.number != 0)
                    a = a.next;
                v.first.next.number = i + 1;
                v = a.target;
            }
        }
    }



    /**
     * Vertex in graph
     */

    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        /**
         * Method returns the degree of vertex.
         *
         * @return the degree of vertex.
         */
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        int number;

        // You can add more fields, if needed

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(", ");
                    sb.append(a.number);
                    sb.append(" )");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Method returns the number of arcs for.
         *
         * @return the number of arcs.
         */
        public int arcs() {
            int number = 0;
            Vertex v = first;
            while (v != null) {
                Arc e = v.first;
                while (e != null) {
                    number++;
                    e = e.next;
                }
                v = v.next;
            }
            return number;
        }

        /**
         * Method returns the beginning point if is Euler graph.
         *
         * @return the beginning vertex.
         */
        //Ma jätan selle koodi nähtavale, sest ma olen tohutult pettunud, et createAdjMatrix() on puhas vale! See ei tagasta õiget asja.
        /*public boolean isEuler() {
            int odds = 0;
            int[][] connected = createAdjMatrix();
            for (int i = 0; i < connected.length; i++) {         //this equals to the row in our matrix.
                for (int j = 0; j < connected[i].length; j++) {   //this equals to the column in each row.
                    System.out.print(connected[i][j] + " ");
                }
                System.out.println();}
            boolean euler = true;
            for (int row = 0; row < connected.length; row++) {
                int sum = 0;
                for (int col = 0; col < connected[row].length; col++) {
                    int value = connected[row][col];
                    System.out.println(value);
                    sum += value;
                }
                System.out.println("Summa " + sum);
                if (sum % 2 == 1) {
                    odds++;
                } else if (sum == 0) {
                    euler = false;
                    break;
                }
            }
            if (odds > 2) {
                euler = false;
            }

            return euler;
        }*/

        public ArrayList<Vertex> vertices() {
            ArrayList<Vertex> vertices = new ArrayList<Vertex>();
            Vertex v = first;
            while (v != null) {
                vertices.add(v);
                Arc a = v.first;
                while (a != null) {
                    a.target.info++;
                    v.info++;
                    a = a.next;
                }
                v = v.next;
            }
            return vertices;
        }
    }
}
